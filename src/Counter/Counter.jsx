import { useState } from "react"
import "./Counter.css"

const Counter = (props) => {
  const [input, setInput] = useState(1)
  const [counter, setCounter] = useState(0)

  return (
    <div>
      <h3 data-testid="title">{props.title}</h3>
      <h1 
        className={`${counter >= 100 ? "green" : ""} ${counter <= -100 ? "red" : "" }`}
        data-testid="counter">{counter}</h1>
      <button
        data-testid="minusBtn"
        onClick={() => setCounter(counter - input)}
      >-</button>
      <input
        className="text-center"
        data-testid="input"
        type="number"
        value={input}
        onChange={(e) => setInput(parseInt(e.target.value)) }
      />
      <button
        data-testid="plusBtn"
        onClick={() => setCounter(input + counter)}
      >+</button>
      <br />
      <button
        data-testid="resetBtn"
        onClick={() => {
          setInput(1)
          setCounter(0)
        }}
      >Reset</button>
    </div>
  )
}

export default Counter
