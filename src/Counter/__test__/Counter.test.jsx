import Counter from "../Counter"
import { render, fireEvent } from "@testing-library/react"

let getByTestId
beforeEach(() => {
  const component = render(<Counter title="Counter Apps" />)
  getByTestId = component.getByTestId
})

test("Ada titlenya", () => {
  // const component = render(<Counter title="Counter Apps" />)
  // const titleEl = component.getByTestId("title")
  // const { getByTestId } = render(<Counter />)
  // const titleEl = getByTestId("title")
  const titleEl = getByTestId("title")
  expect(titleEl.textContent).toBe("Counter Apps")
})

test("Nilai awal counternya adalah 0", () => {
  const counterEl = getByTestId("counter")
  expect(counterEl.textContent).toBe("0")
})

test("Nilai awal inputnya adalah 1", () => {
  const inputEl = getByTestId("input")
  expect(inputEl.value).toBe("1")
})

test("Ada button minus", () => {
  const minusBtn = getByTestId("minusBtn")
  expect(minusBtn.textContent).toBe("-")
})

test("Ada button plus", () => {
  const plusBtn = getByTestId("plusBtn")
  expect(plusBtn.textContent).toBe("+")
})

test("Ada button reset", () => {
  const resetBtn = getByTestId("resetBtn")
  expect(resetBtn.textContent).toBe("Reset")
})

test("Pastikan input berubah", () => {
  const inputEl = getByTestId("input")

  fireEvent.change(inputEl, {
    target: { value: "5" }
  })
  expect(inputEl.value).toBe("5")

  fireEvent.change(inputEl, {
    target: { value: "15" }
  })
  expect(inputEl.value).toBe("15")

  fireEvent.change(inputEl, {
    target: { value: "-42" }
  })
  expect(inputEl.value).toBe("-42")
})

test("Button plus di klik", () => {
  const plusBtn = getByTestId("plusBtn")
  const counterEl = getByTestId("counter")

  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("1")
  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("2")
  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("3")
})

test("Button minus di klik", () => {
  const minusBtn = getByTestId("minusBtn")
  const counterEl = getByTestId("counter")

  fireEvent.click(minusBtn)
  expect(counterEl.textContent).toBe("-1")
  fireEvent.click(minusBtn)
  expect(counterEl.textContent).toBe("-2")
  fireEvent.click(minusBtn)
  expect(counterEl.textContent).toBe("-3")
})

test("Button reset di klik", () => {
  const minusBtn = getByTestId("minusBtn")
  const plusBtn = getByTestId("plusBtn")
  const resetBtn = getByTestId("resetBtn")
  const counterEl = getByTestId("counter")
  const inputEl = getByTestId("input")
  
  fireEvent.click(plusBtn)
  fireEvent.click(plusBtn)
  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("3")

  fireEvent.click(minusBtn)
  expect(counterEl.textContent).toBe("2")

  fireEvent.click(plusBtn)
  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("4")

  fireEvent.click(resetBtn)
  expect(counterEl.textContent).toBe("0")
  expect(inputEl.value).toBe("1")
})

test("Input diubah kemudian di plus dan minus, akhirnya di reset", () => {
  const minusBtn = getByTestId("minusBtn")
  const plusBtn = getByTestId("plusBtn")
  const resetBtn = getByTestId("resetBtn")
  const counterEl = getByTestId("counter")
  const inputEl = getByTestId("input")

  fireEvent.change(inputEl, {
    target: { value: "10" }
  })
  expect(inputEl.value).toBe("10")

  fireEvent.click(minusBtn)
  expect(counterEl.textContent).toBe("-10")
  fireEvent.click(minusBtn)
  expect(counterEl.textContent).toBe("-20")

  fireEvent.change(inputEl, {
    target: { value: "30" }
  })
  expect(inputEl.value).toBe("30")

  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("10")
  fireEvent.click(plusBtn)
  expect(counterEl.textContent).toBe("40")

  fireEvent.click(resetBtn)
  expect(counterEl.textContent).toBe("0")
  expect(inputEl.value).toBe("1")
})

test("counter berwarna hijau", () => {
  const plusBtn = getByTestId("plusBtn")
  const counterEl = getByTestId("counter")
  const inputEl = getByTestId("input")

  fireEvent.change(inputEl, {
    target: { value: "50" }
  })
  fireEvent.click(plusBtn)
  fireEvent.click(plusBtn)
  fireEvent.click(plusBtn)
  expect(counterEl.className).toContain("green")
})

test("counter berwarna merah", () => {
  const minusBtn = getByTestId("minusBtn")
  const counterEl = getByTestId("counter")
  const inputEl = getByTestId("input")

  fireEvent.change(inputEl, {
    target: { value: "50" }
  })
  fireEvent.click(minusBtn)
  fireEvent.click(minusBtn)
  fireEvent.click(minusBtn)
  expect(counterEl.className).toContain("red")
})